import React from "react";
import { CgWorkAlt } from "react-icons/cg";
import { FaReact } from "react-icons/fa";
import { LuGraduationCap } from "react-icons/lu";
import corpcommentImg from "@/public/corpcomment.png";
import rmtdevImg from "@/public/rmtdev.png";
import wordanalyticsImg from "@/public/wordanalytics.png";

export const links = [
  {
    name: "Home",
    hash: "#home",
  },
  {
    name: "About",
    hash: "#about",
  },
  {
    name: "Projects",
    hash: "#projects",
  },
  {
    name: "Skills",
    hash: "#skills",
  },
  {
    name: "Experience",
    hash: "#experience",
  },
  {
    name: "Contact",
    hash: "#contact",
  },
] as const;

export const experiencesData = [
  {
    title: "Full-Stack Development",
    location: "Korea Software HRD Center",
    description:
      "I studied Full-Stack Development at KSHRD for Basic Course And Blockchain for Advanced course",
    icon: React.createElement(LuGraduationCap),
    date: "Jan-2023 - Dec-2023",
  },
  {
    title: "Bachelor's Degree",
    location: "SETEC Institute",
    description:
      "I am studying Management Information System (MIS) at SETEC Institute.",
    icon: React.createElement(LuGraduationCap),
    date: "2022-present",
  },
  {
    title: "Foundation Year",
    location: "Institute of Technology of Cambodia",
    description:
      "I studied 2 years of foundation of Information Technology and Engineering at Institute of Technology of Cambodia",
    icon: React.createElement(LuGraduationCap),

    date: "2020 - 2022",
  },
  {
    title: "Web Development Trainner",
    location: "Z Valley",
    description:
      "I have been trained Web Development at Z Valley Bootcamp for 3 months. I have learned HTML, CSS, JavaScript, PHP, OOP, MVC, Laravel, Git, and more. / I have a little bit worked on Company Exam's System used Laravel and Its Framework.",
    icon: React.createElement(CgWorkAlt),
    date: "Oct-2022 - Dec-2022",
  },
] as const;

export const projectsData = [
  // {
  //   title: "CorpComment",
  //   description:
  //     "I worked as a full-stack developer on this startup project for 2 years. Users can give public feedback to companies.",
  //   tags: ["React", "Next.js", "MongoDB", "Tailwind", "Prisma"],
  //   imageUrl: corpcommentImg,
  // },
  {
    title: "e-Certify",
    description:
      "Korea Software HRD Center / Advanced Course Project /  Verification Certificate Platform",
    tags: [
      "ReactJS + Vite",
      "Tailwind",
      "Redux",
      "Spring Boot",
      "Postgresql",
      "TinyEditor",
      "OneSignal",
      "JsonB",
      "Docker",
      "Blockchain",
      "Hyperledger Fabric",
      "CouchDB",
    ],

    imageUrl: rmtdevImg,
  },
  {
    title: "Portify",
    description:
      "Korea Software HRD Center / Basic Course Project / Social Networking Platform",
    tags: [
      "ReactJS",
      "Tailwind",
      "Redux",
      "Spring Boot",
      "Postgresql",
      "TinyEditor",
      "OneSignal",
      "JsonB",
      "Docker",
    ],
    imageUrl: wordanalyticsImg,
  },
] as const;

export const skillsData = [
  "HTML",
  "CSS",
  "JavaScript",
  "React",
  "Next.js",
  "Git",
  "Tailwind",
  "CouchDB",
  "Redux",
  "PostgreSQL",
  "Python",
  "Spring Boot",
  "MyBatis",
  "Java",
  "Blockchain",
  "Hyperledger Fabric",
  "Docker",
  "JsonB",
  "TinyEditor",
  "OneSignal",
  "Vite",
  "PHP",
  "Laravel",
] as const;
