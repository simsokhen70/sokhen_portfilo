FROM node:18-alpine as builder

WORKDIR /app

COPY package.json .
RUN npm -f install

COPY . .

RUN npm run build

FROM builder as deployment

COPY --from=builder /app/public ./public

EXPOSE 3000

CMD [ "npm", "run", "start" ]
